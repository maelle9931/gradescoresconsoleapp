# README #



# Assignment to make a console app that: #
* takes as a parameter a string that represents a text file containing a list of names, and their scores
* Orders the names by their score. If scores are the same, order by their last name followed by first name
* Creates a new text file called <input-file-name>-graded.txt with the list of sorted score and name

# Running the program #

The executable is named GradeScores.exe and can be found in GradeScores/bin/Release
It must be run from the command line with the score file location as an argument.
For example: GradeScores c:/Files/Scores.txt

The file must have one score per line in the following format:
LastName, FirstName, Score

An example file would contain:

BUNDY, TERESSA, 88

SMITH, ALLAN, 70

KING, MADISON, 88

SMITH, FRANCIS, 85

Empty lines will be ignored.

The app will output the results to the console as well as saving them as a file in the format <filename>-graded.txt

# Building the source code #
This project was created with Visual Studio 2015. The solution is located at GradeScores/GradeScores.sln. To run the code within Visual Studio you will need to add the location of your score file to the command line arguments of the GradeScores project by going to the Debug section of the project properties. 
The unit tests are built with NUnit and are located at GradeScores.Test

